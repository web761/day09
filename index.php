<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

.main {
    font-size: 20px;
    padding: 20px 128px 128px 128px;
}


.main .wrap {
    background-color: #fff;
    padding-top: 48px;
    border-radius: 32px;

}

.wrap .heading {
    text-align: center;
    margin-top: 32px;
}


.wrap .quest-wrap {
    padding: 32px 64px;
}

.wrap #result {
    height: 60vh;
    font-size: 32px;
    text-align: center;
    font-weight: 800;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
}

#result .comment {
    margin-top: 24px;
}

#result .btn-retry {
    padding: 8px;
    background-color: #5fe85a;
    font-size: 20px;
    color: #fff;
    text-decoration: none;
    border-radius: 6px;
    margin-top: 24px;
}

.quest-wrap .page-1,
.quest-wrap .page-2 {
    margin-top: 32px;
    transition: all 1s ease-in;
}


.page-1 .next {
    display: flex;
    align-items: center;
    justify-content: end;
    cursor: pointer;
    margin-top: 32px;

}

.page-1 .next:hover span {
    opacity: 0.9;
}

.next span {
    background-color: #5fe85a;
    color: #fff;
    font-size: 20px;
    padding: 12px;
    border-radius: 4px;
}

.page .quest {
    margin: 24px 0;
}

.quest .ask {
    font-weight: 600;
}

.ask .ask-text {
    font-weight: 600;
    padding: 20px 10px 0px 0px;
}

.quest .answer {
    margin-top: 16px;
}

.answer .input-group {
    margin-top: 8px;
}

.page .page-btn {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;

}

.page-btn>div {
    padding: 12px;
    font-size: 20px;
    text-align: center;
    max-width: 200px;
    border-radius: 6px;
    margin-top: 32px;
    /* margin: 0 128px; */
}

.page-btn>div:hover {
    cursor: pointer;
    opacity: 0.9;
}

.page-btn .submit-btn {
    background-color: #5fe85a;
    color: #fff;
}

.page-btn .back {
    background-color: #e63d50;
    color: #fff;
}

</style>
    <title>Document</title>
</head>

<body>
    <div class="main">
        <div class="wrapper">
            <div class="question-wrapper">
                <div class="page page-1">
                    <div class="question-list">

                    </div>
                    <div class="next"><span>Tiếp theo</span></div>
                </div>
                <div class="page page-2" style="display: none;">
                    <div class="question-list">

                    </div>
                    <div class="page-btn">
                        <div class="back">Quay lại trang trước</div>
                        <div class="submit-btn">Nộp bài</div>
                    </div>
                </div>
            </div>
            <div id="result" style="display:none">
            </div>
        </div>

    </div>
    <script>
        const quiz = [{
                question: "1 + 1 =",
                choices: [
                    "1",
                    "2",
                    "3",
                    "4",
                ],
                answer: "2"
            },
            {
                question: "3 + 3 = ",
                choices: [
                    "5",
                    "6",
                    "7",
                    "6.5",
                ],
                answer: "2"
            },
            {
                question: "1 - x = 2",
                choices: [
                    "x = 1",
                    "x = 2",
                    "x = 3",
                    "x = -1",
                ],
                answer: "4"
            },
            {
                question: "x^2 = 4",
                choices: [
                    "x = +-2",
                    "x = +-3",
                    "x = +-4",
                    "x = +-5",
                ],
                answer: "1"
            },
            {
                question: "5 + 5 =",
                choices: [
                    "8",
                    "9",
                    "10",
                    "11",
                ],
                answer: "3"
            },

            {
                question: "5 * 5 =",
                choices: [
                    "15",
                    "20",
                    "25",
                    "30",
                ],
                answer: "3"
            },
            {
                question: "7 + 7 = ",
                choices: [
                    "9",
                    "14",
                    "44",
                    "17",
                ],
                answer: "2"
            },
            {
                question: "11 * 2 =",
                choices: [
                    "68",
                    "72",
                    "22",
                    "11",
                ],
                answer: "3"
            },
            {
                question: "9 * 9 =",
                choices: [
                    "81",
                    "72",
                    "63",
                    "54",
                ],
                answer: "1"
            },
            {
                question: "6 * 6 =",
                choices: [
                    "54",
                    "48",
                    "42",
                    "36",
                ],
                answer: "4"
            },

        ]
        renderQuestion(quiz);
        var answers = document.getElementsByName("answer")
        var answerForms = document.querySelectorAll('.answerForm');
        var result = document.getElementById('result');
        var score = 0;
        const badScore = "Bạn quá kém, cần ôn tập thêm"
        const mediumScore = "Cũng bình thường"
        const goodScore = "Sắp sửa làm được trợ giảng lớp PHP"

        function checkResult(score) {
            if (score < 4) {
                result.innerHTML += '<span class="comment" style="color:#dc3545 ;">' + badScore + '</span>';
            } else if (score >= 4 && score <= 7) {
                result.innerHTML += '<span class="comment" style="color: #ffc107;">' + mediumScore + '</span>';
            } else if (score > 7) {
                result.innerHTML += '<span class="comment" style="color:#28a745 ;">' + goodScore + '</span>';
            }
        }
        answerForms.forEach((answerForm, index) => {
            let answer = answerForm.elements.length
            answerForm.onchange = function() {
                for (let i = 0; i < answerForm.elements.length; i++) {
                    const element = answerForm.elements[i];
                    var userChoice = Number(element.value) + 1;

                    if (element.checked == true) {
                        if (userChoice === Number(quiz[index].answer)) {
                            score++;
                        } 
                    } 
                }
            }
        })
        function renderPage(i, data, index) {
            var page = document.querySelector(`.page-${i} .question-list`);
            var ask = ` <div class="ask">
                            <p class="ask-text">
                                <span class="question-order">Câu ${index+1}: ${data.question }</span>
                            </p>
                        </div>`
            var answers = ``
            data.choices.forEach((choice, num) => {
                answers += `<div class="input-group">
                                    <input type="radio" name="answer" value="${num}" id="answer_1">
                                    <label class="form-label">${choice}</label>
                                </div>`
            })
            page.innerHTML += `<div class="question">` + ask +`<div class="answer"><form action="" class="answerForm">` + answers +`</form>
                                </div>
                                </div>`
        }

        function renderQuestion(quiz) {
            quiz.forEach((question, index) => {
                if (index <= 4) {
                    renderPage(1, question, index)
                } else {renderPage(2, question, index)}
            })
        }
        var submitBtn = document.querySelector('.submit-btn');
        submitBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'none';
            result.style.display = 'flex'
            result.innerHTML = "Score: " + score + "/" + quiz.length + "</br>";
            checkResult(score)
        }
        var page1 = document.querySelector('.page-1');
        var page2 = document.querySelector('.page-2');
        var nextPageBtn = document.querySelector('.next');
        nextPageBtn.onclick = function() {
            page1.style.display = 'none';
            page2.style.display = 'block';
        }
        var backBtn = document.querySelector('.back');
        backBtn.onclick = function() {
            page2.style.display = 'none';
            page1.style.display = 'block';
        }
    </script>
</body>

</html>
