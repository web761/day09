<?php
$answer = array(
    1 => "A",
    2 => "B",
    3 => "C",
    4 => "D",
    5 => "A",
    6 => "B",
    7 => "C",
    8 => "D",
    9 => "D",
    10 => "A"
);

$score = 0;
if (isset($_COOKIE["cau1"])) {
    for ($i = 0; $i < 10; $i++) {
        if ($_COOKIE["cau". $i + 1 .""] == $answer[$i+1]) {
            $score ++;
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
    <link rel="stylesheet" href="ThemeResult.css">
</head>
<body>
    <div class="wrapper">
        <?php
            if ($score < 4) {
                echo "
                    <label>$score điểm</label> <br>
                    <label>Bạn quá kém cần ôn tập thêm</label>
                ";
            } else if ($score > 4 && $score < 7){
                echo "
                    <label>$score điểm</label> <br>
                    <label>Cũng bình thường</label>
                ";   
            } else {
                echo "
                    <label>$score điểm</label> <br>
                    <label>Sắp sửa làm trợ giảng lớp PHP</label>
                ";
            }
        ?> 
    
    </div>
</body>
</html>
